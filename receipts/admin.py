from django.contrib import admin
from receipts.models import Account, ExpenseCategory, Receipt
# Register your models here.


@admin.register(Account)
class Account(admin.ModelAdmin):
    pass


@admin.register(ExpenseCategory)
class ExpenseCategory(admin.ModelAdmin):
    pass


@admin.register(Receipt)
class Receipt(admin.ModelAdmin):
    pass
