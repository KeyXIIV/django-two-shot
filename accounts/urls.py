from django.urls import path
from accounts.views import log_in, user_logout, signup

urlpatterns = [
    path("login/", log_in, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup, name="signup"),
]
